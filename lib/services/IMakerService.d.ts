import { TTakeResult } from '.';
import { TNewMaker } from '../models/maker/types';
export interface IMakerService {
    initializeParams(makerProps: TNewMaker): TNewMaker;
    takeUnits(makerId: string, takeSize: number): Promise<TTakeResult | null>;
}
