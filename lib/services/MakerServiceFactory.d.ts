import { TTakeResult } from './types';
import { IMakerService } from './IMakerService';
import { TNewMaker } from '../models/maker/types';
export declare class MakerServiceFactory implements IMakerService {
    private db;
    private makerRepository;
    constructor(db: FirebaseFirestore.Firestore);
    initializeParams(makerProps: TNewMaker): TNewMaker;
    takeUnits(assetId: string, takeSize: number): Promise<TTakeResult | null>;
}
