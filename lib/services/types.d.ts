export declare type TTakeResult = {
    bid: number;
    ask: number;
    last: number;
    makerDeltaUnits: number;
    makerDeltaCoins: number;
};
