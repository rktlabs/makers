import { TTakeResult } from '../../services/types';
import { IMakerService } from '../../services/IMakerService';
import { TMakerParams } from './types';
import { TNewMaker } from '../../models/maker/types';
export declare class KMakerService implements IMakerService {
    private makerRepository;
    private parmUpdater;
    constructor(db: FirebaseFirestore.Firestore);
    initializeParams(makerProps: TNewMaker): {
        params: TMakerParams;
        type: string;
        ownerId: string;
        assetId: string;
        settings?: any;
    };
    takeUnits(makerId: string, takeSize: number): Promise<TTakeResult | null>;
    private computePrice;
}
