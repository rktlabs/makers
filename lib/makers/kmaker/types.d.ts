export declare type TMakerParamsUpdate = {
    poolUnitDelta: number;
    poolCoinDelta: number;
    kDelta: number;
    madeUnitsDelta: number;
    currentPrice: number;
};
export declare type TMakerParams = {
    x0: number;
    poolUnits: number;
    poolCoins: number;
    k: number;
};
