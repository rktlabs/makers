export declare type TMakerParamsUpdate = {
    madeUnitsDelta: number;
    currentPrice: number;
};
export declare type TMakerParams = {
    price0: number;
    coinPool: number;
    limit: number;
    a: number;
    k: number;
};
