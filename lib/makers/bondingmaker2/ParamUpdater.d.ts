import { TMakerParamsUpdate } from './types';
export declare class ParamUpdater {
    db: FirebaseFirestore.Firestore;
    constructor(db: FirebaseFirestore.Firestore);
    updateMakerParams(makerId: string, makerPropsUpdate: TMakerParamsUpdate): Promise<void>;
}
