export declare type TMakerParamsUpdate = {
    madeUnitsDelta: number;
    currentPrice: number;
};
export declare type TMakerParams = {
    x0: number;
};
