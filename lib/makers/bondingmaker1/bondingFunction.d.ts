import { TMakerParams } from './types';
export declare const bondingFunction: (x: number, params: TMakerParams) => number;
export declare const inverseBondingFunction: (currentPrice: number, madeUnits: number) => TMakerParams;
