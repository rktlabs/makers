import { TMaker, TNewMaker } from './types';
export declare class Maker {
    createdAt: string;
    type: string;
    ownerId: string;
    assetId: string;
    portfolioId?: string;
    madeUnits: number;
    currentPrice?: number;
    params?: any;
    constructor(props: TMaker);
    static newMaker(props: TNewMaker): Maker;
    static serialize(req: any, data: any): any;
    static serializeCollection(req: any, data: any): any;
}
