import { Maker } from '../models/maker';
import { TMaker, TMakerPatch } from '../models/maker/types';
export declare class MakerRepository {
    db: FirebaseFirestore.Firestore;
    constructor(db: FirebaseFirestore.Firestore);
    listMakers(qs?: any): Promise<TMaker[]>;
    getMaker(assetId: string): Promise<TMaker | null>;
    storeMaker(entity: Maker): Promise<void>;
    deleteMaker(assetId: string): Promise<void>;
    updateMaker(entityId: string, entityData: TMakerPatch): Promise<void>;
}
