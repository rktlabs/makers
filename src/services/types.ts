'use strict'

export type TTakeResult = {
    bid: number
    ask: number
    last: number
    makerDeltaUnits: number
    makerDeltaCoins: number
}
