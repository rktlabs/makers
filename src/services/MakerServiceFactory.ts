'use strict'
import { TTakeResult } from './types'
import { IMakerService } from './IMakerService'
import { MakerRepository } from '..'
import { TNewMaker } from '../models/maker/types'
import { KMakerService } from '../makers/kmaker'
import { BondingMaker1Service } from '../makers/bondingmaker1'
import { BondingMaker2Service } from '../makers/bondingmaker2'
import { LogisticMaker1Service } from '../makers/logisticmaker1'

export class MakerServiceFactory implements IMakerService {
    private db: FirebaseFirestore.Firestore
    private makerRepository: MakerRepository

    constructor(db: FirebaseFirestore.Firestore) {
        this.db = db
        this.makerRepository = new MakerRepository(db)
    }

    initializeParams(makerProps: TNewMaker): TNewMaker {
        switch (makerProps.type) {
            case 'constantk':
                const kMakerService = new KMakerService(this.db)
                return kMakerService.initializeParams(makerProps)

            case 'bondingmaker1':
                const bmakerService1 = new BondingMaker1Service(this.db)
                return bmakerService1.initializeParams(makerProps)

            case 'bondingmaker2':
                const bmakerService2 = new BondingMaker2Service(this.db)
                return bmakerService2.initializeParams(makerProps)

            case 'logisticmaker1':
                const logisticmakerService1 = new LogisticMaker1Service(this.db)
                return logisticmakerService1.initializeParams(makerProps)

            default:
                const defaultMakerService = new KMakerService(this.db)
                return defaultMakerService.initializeParams(makerProps)
        }
    }

    async takeUnits(assetId: string, takeSize: number): Promise<TTakeResult | null> {
        // have to get the maker to get the type. Get if from a "plain" repo
        const maker = await this.makerRepository.getMaker(assetId)
        if (!maker) {
            return null
        }

        switch (maker.type) {
            case 'constantk':
                const kMakerService = new KMakerService(this.db)
                return kMakerService.takeUnits(assetId, takeSize)

            case 'bondingmaker1':
                const bmakerService1 = new BondingMaker1Service(this.db)
                return bmakerService1.takeUnits(assetId, takeSize)

            case 'bondingmaker2':
                const bmakerService2 = new BondingMaker2Service(this.db)
                return bmakerService2.takeUnits(assetId, takeSize)

            case 'logisticmaker1':
                const logisticmakerService1 = new LogisticMaker1Service(this.db)
                return logisticmakerService1.takeUnits(assetId, takeSize)

            default:
                const defaultMakerService = new KMakerService(this.db)
                return defaultMakerService.takeUnits(assetId, takeSize)
        }
    }
}
