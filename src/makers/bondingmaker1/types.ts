'use strict'

export type TMakerParamsUpdate = {
    madeUnitsDelta: number
    currentPrice: number
}

export type TMakerParams = {
    x0: number
}
