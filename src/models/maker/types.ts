export type TNewMaker = {
    type: string
    ownerId: string
    assetId: string

    settings?: any

    params?: any
}

export type TMaker = {
    ownerId: string
    createdAt: string
    type: string
    assetId: string
    portfolioId?: string

    madeUnits: number
    currentPrice?: number

    params?: any
}

export type TMakerPatch = {
    currentPrice?: number
    params?: any
}
