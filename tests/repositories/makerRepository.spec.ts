'use strict'
/* eslint-env node, mocha */

import { expect } from 'chai'

import * as firebase from 'firebase-admin'

import { Maker } from '../../src/models'
import { MakerRepository } from '../../src/repositories'

describe('Maker Repository', () => {
    let repo: MakerRepository
    const assetId = 'card::test1'

    before(async () => {
        const db = firebase.firestore()
        repo = new MakerRepository(db)
    })

    afterEach(async () => {
        // clean out records.
        await repo.deleteMaker(assetId)
    })

    describe('Create Basic Maker', () => {
        it('should create', async () => {
            const data = {
                type: 'constantk',
                assetId: assetId,
                ownerId: 'tester',
                initPrice: 10,
                params: {
                    param1: 100,
                    param2: 11,
                },
            }

            const maker = Maker.newMaker(data)
            await repo.storeMaker(maker)

            const readBack = await repo.getMaker(assetId)
            expect(readBack).to.exist
            expect(readBack!!.params).to.exist
            expect(readBack!!.params.param2).to.eq(11)
        })
    })

    describe('Create Full Maker', () => {
        it('should create', async () => {
            const data = {
                type: 'constantk',
                assetId: assetId,
                ownerId: 'tester',
                initPrice: 10,
                params: {
                    param1: 100,
                    param2: 11,
                },
            }

            const maker = Maker.newMaker(data)
            await repo.storeMaker(maker)

            const readBack = await repo.getMaker(assetId)
            expect(readBack).to.exist
            if (readBack) {
                expect(readBack.type).to.eq('constantk')
                expect(readBack.ownerId).to.eq('tester')
                expect(readBack.assetId).to.eq(assetId)

                expect(readBack.params).to.exist
                expect(readBack.params.param2).to.eq(11)
            }
        })
    })
})
