'use strict'
/* eslint-env node, mocha */

import { expect } from 'chai'
import * as firebase from 'firebase-admin'
import { Maker, MakerRepository, MakerServiceFactory, TNewMaker } from '../../../src'

describe('Maker Service', function () {
    let db: FirebaseFirestore.Firestore
    let makerRepository: MakerRepository

    before(async () => {
        db = firebase.firestore()
        makerRepository = new MakerRepository(db)
    })

    describe('Init ConstantK maker params', () => {
        it('should create', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'constantk', // <=== type constantK initiaizer
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 10,
                },
            }

            const newProps = makerFactory.initializeParams(props)

            expect(newProps).to.not.eq(props)
            expect(newProps.params).to.exist

            expect(newProps.params.poolUnits).to.eq(1000)
            expect(newProps.params.poolCoins).to.eq(10000)
            expect(newProps.params.k).to.eq(10000000)
            //expect(newProps.params.madeUnits).to.eq(0)
        })
    })

    describe('new Maker init price 500 initial Units 100', () => {
        it('should create init price 10', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'constantk', // <=== type constantK initiaizer
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 500,
                    initMadeUnits: 100,
                },
            }

            const newProps = makerFactory.initializeParams(props)

            expect(newProps).to.not.eq(props)
            expect(newProps.params).to.exist

            expect(newProps.params.poolUnits).to.eq(900)
            expect(newProps.params.poolCoins).to.eq(450000)
            expect(newProps.params.k).to.eq(405000000)
            //expect(newProps.params.madeUnits).to.eq(100)
            expect(newProps.params.x0).to.eq(1000)
        })
    })

    describe('Init ConstantK maker params', () => {
        it('should create', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'constantk', // <=== type constantK initiaizer
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 1,
                },
            }

            const newProps = makerFactory.initializeParams(props)

            expect(newProps).to.not.eq(props)
            expect(newProps.params).to.exist

            expect(newProps.params.poolUnits).to.eq(1000)
            expect(newProps.params.poolCoins).to.eq(1000)
            expect(newProps.params.k).to.eq(1000000)
            //expect(newProps.params.madeUnits).to.eq(0)
            expect(newProps.params.x0).to.eq(1000)
        })
    })

    describe('Take 1 from ConstantK', () => {
        it('should create', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'constantk',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 10,
                },
            }

            const newProps = makerFactory.initializeParams(props)
            const maker = Maker.newMaker(newProps)
            await makerRepository.storeMaker(maker)

            expect(newProps!!.params!!.poolUnits).to.eq(1000)
            expect(newProps!!.params!!.poolCoins).to.eq(10000)
            expect(newProps!!.params!!.k).to.eq(10000000)
            //expect(newProps!!.madeUnits).to.eq(0)
            expect(newProps!!.params.x0).to.eq(1000)

            // buy 5 units - gets new quote data and deltas
            const takeResult = await makerFactory.takeUnits('card::assetId', 1)

            expect(takeResult).to.exist
            expect(takeResult!!.bid).to.eq(10)
            expect(takeResult!!.last).to.eq(10)
            expect(takeResult!!.ask).to.eq(10.02)
            expect(takeResult!!.makerDeltaUnits).to.eq(-1)
            expect(takeResult!!.makerDeltaCoins).to.eq(10.01)

            // and updates the maker params
            const updatedMaker = await makerRepository.getMaker('card::assetId')
            expect(updatedMaker).to.exist
            expect(updatedMaker!!.params!!).to.exist
            expect(updatedMaker!!.params!!.poolUnits).to.eq(999)
            expect(updatedMaker!!.params!!.poolCoins).to.eq(10010.01)
            expect(updatedMaker!!.params!!.k).to.eq(10000000)
            expect(updatedMaker!!.madeUnits).to.eq(1)
        })
    })

    describe('Take 100 from ConstantK', () => {
        it('should create', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'constantk',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 1,
                },
            }

            const newProps = makerFactory.initializeParams(props)
            const maker = Maker.newMaker(newProps)
            await makerRepository.storeMaker(maker)

            expect(newProps!!.params!!.poolUnits).to.eq(1000)
            expect(newProps!!.params!!.poolCoins).to.eq(1000)
            expect(newProps!!.params!!.k).to.eq(1000000)
            //expect(newProps!!.params!!.madeUnits).to.eq(0)

            const takeResult = await makerFactory.takeUnits('card::assetId', 100)

            expect(takeResult).to.exist
            expect(takeResult!!.bid).to.eq(1.2318)
            expect(takeResult!!.ask).to.eq(1.2346)
            expect(takeResult!!.last).to.eq(1.2318)
            expect(takeResult!!.makerDeltaUnits).to.eq(-100)
            expect(takeResult!!.makerDeltaCoins).to.eq(111.1111)

            // and updates the maker params
            const updatedMaker = await makerRepository.getMaker('card::assetId')
            expect(updatedMaker).to.exist
            expect(updatedMaker!!.params!!).to.exist
            expect(updatedMaker!!.params!!.poolUnits).to.eq(900)
            expect(updatedMaker!!.params!!.poolCoins).to.eq(1111.1111)
            expect(updatedMaker!!.params!!.k).to.eq(1000000)
            expect(updatedMaker!!.madeUnits).to.eq(100)
        })
    })

    describe('Take 0 from constantk', () => {
        it('should create', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'constantk',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 2,
                },
            }

            const newProps = makerFactory.initializeParams(props)
            const maker = Maker.newMaker(newProps)
            await makerRepository.storeMaker(maker)

            expect(newProps!!.params!!.poolUnits).to.eq(1000)
            expect(newProps!!.params!!.poolCoins).to.eq(2000)
            expect(newProps!!.params!!.k).to.eq(2000000)
            //expect(newProps!!.params!!.madeUnits).to.eq(0)

            const takeResult = await makerFactory.takeUnits('card::assetId', 0)

            expect(takeResult).to.exist
            expect(takeResult!!.ask).to.eq(2)
            expect(takeResult!!.bid).to.eq(1.996)
            expect(takeResult!!.last).to.eq(1.996)
            expect(takeResult!!.makerDeltaUnits).to.eq(0)
            expect(takeResult!!.makerDeltaCoins).to.eq(0)

            // and updates the maker params
            const updatedMaker = await makerRepository.getMaker('card::assetId')
            expect(updatedMaker).to.exist
            expect(updatedMaker!!.params!!).to.exist

            expect(updatedMaker!!.params!!.poolUnits).to.eq(1000)
            expect(updatedMaker!!.params!!.poolCoins).to.eq(2000)
            expect(updatedMaker!!.params!!.k).to.eq(2000000)
            expect(updatedMaker!!.madeUnits).to.eq(0)
        })
    })
})
