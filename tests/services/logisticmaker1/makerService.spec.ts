'use strict'
/* eslint-env node, mocha */

import { expect } from 'chai'
import * as firebase from 'firebase-admin'
import { Maker, MakerRepository, MakerServiceFactory, TNewMaker } from '../../../src'

describe.only('Maker Service', function () {
    this.timeout(5000)
    let db: FirebaseFirestore.Firestore
    let makerRepository: MakerRepository

    before(async () => {
        db = firebase.firestore()
        makerRepository = new MakerRepository(db)
    })

    describe('new Maker init price 1', () => {
        it('should create init price 1', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'logisticmaker1',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 1,
                    limit: 640,
                    coinPool: 1500 * 10000,
                },
            }

            const newProps = makerFactory.initializeParams(props)
            expect(newProps).to.not.eq(props)
            expect(newProps.params).to.exist
            expect(newProps.params.limit).to.eq(640)
            expect(newProps.params.a).to.be.eq(639)
            expect(newProps.params.k).to.be.closeTo(0.0003918859292770133, 0.0000000000001)
            expect(newProps.params.price0).to.eq(1)
        })
    })

    describe('new Maker init price 2', () => {
        it('should create', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'logisticmaker1',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 2,
                    limit: 640,
                    coinPool: 1500 * 10000,
                },
            }

            const newProps = makerFactory.initializeParams(props)
            expect(newProps.params.limit).to.eq(640)
            expect(newProps.params.a).to.be.eq(319)
            expect(newProps.params.k).to.be.closeTo(0.0003918859292770133, 0.0000000000001)
            expect(newProps.params.price0).to.eq(2)
        })
    })

    describe('initialize for price and unitsMade', () => {
        it('should initialize', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const madeUnits = 15000
            const props: TNewMaker = {
                type: 'logisticmaker1',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 229.47900698318927,
                    limit: 640,
                    coinPool: 1500 * 10000,
                    initMadeUnits: madeUnits,
                },
            }
            const newProps = makerFactory.initializeParams(props)

            expect(newProps.params.limit).to.eq(640)
            expect(newProps.params.a).to.be.closeTo(639, 0.0000000001)
            expect(newProps.params.k).to.be.closeTo(0.0003918859292770133, 0.0000000000001)
            expect(newProps.params.price0).to.eq(1)
        })
    })

    describe('initialize for price and unitsMade', () => {
        it('should initialize', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const madeUnits = 15000
            const props: TNewMaker = {
                type: 'logisticmaker1',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 229.47900698318927,
                    limit: 640,
                    coinPool: 2000 * 10000,
                    initMadeUnits: madeUnits,
                },
            }
            const newProps = makerFactory.initializeParams(props)

            expect(newProps.params.limit).to.eq(640)
            expect(newProps.params.a).to.be.closeTo(146.98524676345244, 0.0000000001)
            expect(newProps.params.k).to.be.closeTo(0.00029391444695775997, 0.0000000000001)
            expect(newProps.params.price0).to.eq(4.324755433377831)
        })
    })

    describe('new Maker init price 10', () => {
        it('should create init price 10', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'logisticmaker1', // <=== type constantK initiaizer
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 10,
                    limit: 640,
                    coinPool: 1500 * 10000,
                },
            }

            const newProps = makerFactory.initializeParams(props)

            expect(newProps).to.not.eq(props)
            expect(newProps.params).to.exist

            expect(newProps.params.price0).to.eq(10)
        })
    })

    describe('new Maker init price 500 initial Units 100', () => {
        it('should create init price 10', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'logisticmaker1', // <=== type constantK initiaizer
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 500,
                    limit: 640,
                    coinPool: 1500 * 10000,
                    initMadeUnits: 100,
                },
            }

            const newProps = makerFactory.initializeParams(props)

            expect(newProps).to.not.eq(props)
            expect(newProps.params).to.exist

            expect(newProps.params.price0).to.eq(495.6665397287473)
        })
    })

    describe('Take 1 from LogisicFunction1', () => {
        it('should create', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'logisticmaker1',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 1,
                    limit: 640,
                    coinPool: 1500 * 10000,
                },
            }

            const newProps = makerFactory.initializeParams(props)
            const maker = Maker.newMaker(newProps)
            await makerRepository.storeMaker(maker)

            // // buy 5 units - gets new quote data and deltas
            const takeResult = await makerFactory.takeUnits('card::assetId', 1)

            expect(takeResult).to.exist
            expect(takeResult!!.bid).to.eq(1)
            expect(takeResult!!.ask).to.eq(1.00039135004516)
            expect(takeResult!!.last).to.eq(1)
            expect(takeResult!!.makerDeltaCoins).to.eq(1)
            expect(takeResult!!.makerDeltaUnits).to.eq(-1)

            // and updates the maker params
            const updatedMaker = await makerRepository.getMaker('card::assetId')
            expect(updatedMaker).to.exist
            expect(updatedMaker!!.params!!).to.exist
            expect(updatedMaker!!.madeUnits).to.eq(1)
            expect(updatedMaker!!.params!!.price0).to.eq(1)
        })
    })

    describe('Take 100 from LogisicFunction1', () => {
        it('should create', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'logisticmaker1',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 1,
                    limit: 640,
                    coinPool: 1500 * 10000,
                },
            }

            const newProps = makerFactory.initializeParams(props)
            const maker = Maker.newMaker(newProps)
            await makerRepository.storeMaker(maker)

            const takeResult = await makerFactory.takeUnits('card::assetId', 100)

            expect(takeResult).to.exist
            expect(takeResult!!.bid).to.eq(1.0394948747128472)
            expect(takeResult!!.ask).to.eq(1.0399016559255607)
            expect(takeResult!!.last).to.eq(1.0394948747128472)
            expect(takeResult!!.makerDeltaUnits).to.eq(-100)
            expect(takeResult!!.makerDeltaCoins).to.eq(101.9621)

            // and updates the maker params
            const updatedMaker = await makerRepository.getMaker('card::assetId')
            expect(updatedMaker).to.exist
            expect(updatedMaker!!.params!!).to.exist
            expect(updatedMaker!!.madeUnits).to.eq(100)
            expect(updatedMaker!!.params!!.price0).to.eq(1)
        })
    })

    describe('Take 15000 from LogisicFunction1', () => {
        it('should create', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'logisticmaker1',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 1,
                    limit: 640,
                    coinPool: 1500 * 10000,
                },
            }

            const newProps = makerFactory.initializeParams(props)
            const maker = Maker.newMaker(newProps)
            await makerRepository.storeMaker(maker)

            const takeResult = await makerFactory.takeUnits('card::assetId', 15000)

            expect(takeResult).to.exist
            expect(takeResult!!.bid).to.eq(229.42132582765527)
            expect(takeResult!!.ask).to.eq(229.47900698318927)
            expect(takeResult!!.last).to.eq(229.42132582765527)
            expect(takeResult!!.makerDeltaUnits).to.eq(-15000)
            expect(takeResult!!.makerDeltaCoins).to.eq(722508.1059)

            // and updates the maker params
            const updatedMaker = await makerRepository.getMaker('card::assetId')
            expect(updatedMaker).to.exist
            expect(updatedMaker!!.params!!).to.exist
            expect(updatedMaker!!.madeUnits).to.eq(15000)
            expect(updatedMaker!!.params!!.price0).to.eq(1)
        })
    })

    describe('Take 0 from LogisicFunction1', () => {
        it('should create', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'logisticmaker1',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 2,
                    limit: 640,
                    coinPool: 1500 * 10000,
                },
            }

            const newProps = makerFactory.initializeParams(props)
            const maker = Maker.newMaker(newProps)
            await makerRepository.storeMaker(maker)
            expect(newProps.params.limit).to.eq(640)
            expect(newProps.params.a).to.be.eq(319)
            expect(newProps.params.k).to.be.closeTo(0.0003918859292770133, 0.0000000001)
            expect(newProps.params.price0).to.eq(2)

            const takeResult = await makerFactory.takeUnits('card::assetId', 0)

            expect(takeResult).to.exist
            expect(takeResult!!.ask).to.eq(2)
            expect(takeResult!!.bid).to.eq(1.9992188295467006)
            expect(takeResult!!.last).to.eq(1.9992188295467006)
            expect(takeResult!!.makerDeltaUnits).to.eq(0)
            expect(takeResult!!.makerDeltaCoins).to.eq(0)

            // and updates the maker params
            const updatedMaker = await makerRepository.getMaker('card::assetId')
            expect(updatedMaker).to.exist
            expect(updatedMaker!!.params!!).to.exist
            expect(updatedMaker!!.madeUnits).to.eq(0)
            expect(updatedMaker!!.params!!.price0).to.eq(2)
        })
    })

    describe('new Maker init price 500 initial Units 100', () => {
        it('should create init price 10', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'logisticmaker1',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 1,
                    limit: 640,
                    coinPool: 1500 * 10000,
                },
            }

            const newProps = makerFactory.initializeParams(props)
            const maker = Maker.newMaker(newProps)
            await makerRepository.storeMaker(maker)

            const takeResult = await makerFactory.takeUnits('card::assetId', 15000)

            const takeBackResult = await makerFactory.takeUnits('card::assetId', -1000)
            expect(takeBackResult!!.makerDeltaUnits).to.be.greaterThan(0)
        })
    })
})
