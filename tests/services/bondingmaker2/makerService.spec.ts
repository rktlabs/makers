'use strict'
/* eslint-env node, mocha */

import { expect } from 'chai'
import * as firebase from 'firebase-admin'
import { Maker, MakerRepository, MakerServiceFactory, TNewMaker } from '../../../src'

describe('Maker Service', function () {
    let db: FirebaseFirestore.Firestore
    let makerRepository: MakerRepository

    before(async () => {
        db = firebase.firestore()
        makerRepository = new MakerRepository(db)
    })

    describe('new Maker init price 10', () => {
        it('should create init price 10', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'bondingmaker2', // <=== type constantK initiaizer
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 10,
                },
            }

            const newProps = makerFactory.initializeParams(props)

            expect(newProps).to.not.eq(props)
            expect(newProps.params).to.exist

            //expect(newProps.params.madeUnits).to.eq(0)
            expect(newProps.params.x0).to.eq(9)
        })
    })

    describe('new Maker init price 500 initial Units 100', () => {
        it('should create init price 10', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'bondingmaker2', // <=== type constantK initiaizer
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 500,
                    initMadeUnits: 100,
                },
            }

            const newProps = makerFactory.initializeParams(props)

            expect(newProps).to.not.eq(props)
            expect(newProps.params).to.exist

            //expect(newProps.params.madeUnits).to.eq(100)
            expect(newProps.params.x0).to.eq(399)
        })
    })

    describe('new Maker init price 1', () => {
        it('should create init price 1', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'bondingmaker2',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 1,
                },
            }

            const newProps = makerFactory.initializeParams(props)
            expect(newProps).to.not.eq(props)
            expect(newProps.params).to.exist
            //expect(newProps.params.madeUnits).to.eq(0)
            expect(newProps.params.x0).to.eq(0)
        })
    })

    describe('Take 1 from BundingFunction2', () => {
        it('should create', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'bondingmaker2',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 10,
                },
            }

            const newProps = makerFactory.initializeParams(props)
            const maker = Maker.newMaker(newProps)
            await makerRepository.storeMaker(maker)

            // // buy 5 units - gets new quote data and deltas
            const takeResult = await makerFactory.takeUnits('card::assetId', 1)

            expect(takeResult).to.exist
            expect(takeResult!!.bid).to.eq(10)
            expect(takeResult!!.ask).to.eq(11)
            expect(takeResult!!.last).to.eq(10)
            expect(takeResult!!.makerDeltaCoins).to.eq(10)
            expect(takeResult!!.makerDeltaUnits).to.eq(-1)

            // and updates the maker params
            const updatedMaker = await makerRepository.getMaker('card::assetId')
            expect(updatedMaker).to.exist
            expect(updatedMaker!!.params!!).to.exist
            expect(updatedMaker!!.madeUnits).to.eq(1)
            expect(updatedMaker!!.params!!.x0).to.eq(9)
        })
    })

    describe('Take 100 from BundingFunction2', () => {
        it('should create', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'bondingmaker2',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 1,
                },
            }

            const newProps = makerFactory.initializeParams(props)
            const maker = Maker.newMaker(newProps)
            await makerRepository.storeMaker(maker)

            const takeResult = await makerFactory.takeUnits('card::assetId', 100)

            expect(takeResult).to.exist
            expect(takeResult!!.bid).to.eq(100)
            expect(takeResult!!.ask).to.eq(101)
            expect(takeResult!!.last).to.eq(100)
            expect(takeResult!!.makerDeltaUnits).to.eq(-100)
            expect(takeResult!!.makerDeltaCoins).to.eq(5050)

            // and updates the maker params
            const updatedMaker = await makerRepository.getMaker('card::assetId')
            expect(updatedMaker).to.exist
            expect(updatedMaker!!.params!!).to.exist
            expect(updatedMaker!!.madeUnits).to.eq(100)
            expect(updatedMaker!!.params!!.x0).to.eq(0)
        })
    })

    describe('Take 0 from BundingFunction2', () => {
        it('should create', async () => {
            const makerFactory = new MakerServiceFactory(db)

            const props: TNewMaker = {
                type: 'bondingmaker2',
                ownerId: 'owner',
                assetId: 'card::assetId',
                settings: {
                    initPrice: 2,
                },
            }

            const newProps = makerFactory.initializeParams(props)
            const maker = Maker.newMaker(newProps)
            await makerRepository.storeMaker(maker)
            //expect(newProps!!.params!!.madeUnits).to.eq(0)
            expect(newProps!!.params!!.x0).to.eq(1)

            const takeResult = await makerFactory.takeUnits('card::assetId', 0)

            expect(takeResult).to.exist
            expect(takeResult!!.ask).to.eq(2)
            expect(takeResult!!.bid).to.eq(1)
            expect(takeResult!!.last).to.eq(1)
            expect(takeResult!!.makerDeltaUnits).to.eq(0)
            expect(takeResult!!.makerDeltaCoins).to.eq(0)

            // and updates the maker params
            const updatedMaker = await makerRepository.getMaker('card::assetId')
            expect(updatedMaker).to.exist
            expect(updatedMaker!!.params!!).to.exist
            expect(updatedMaker!!.madeUnits).to.eq(0)
            expect(updatedMaker!!.params!!.x0).to.eq(1)
        })
    })
})
