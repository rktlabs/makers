'use strict'
/* eslint-env node, mocha */
import { expect } from 'chai'

import { Maker, TNewMaker } from '../../../src/models'

describe('Maker', () => {
    const assetId = 'card::the.card'

    describe('Create New Maker', () => {
        it('generate typed assetId if assetId not supplied', async () => {
            const data: TNewMaker = {
                type: 'constantk',
                ownerId: 'tester',
                assetId: assetId,
                settings: {
                    initPrice: 10,
                },
            }

            const maker = Maker.newMaker(data)
            expect(maker.assetId).is.eq(assetId)
            expect(maker.type).is.eq('constantk')
            expect(maker.portfolioId).to.not.exist

            // expect(maker.bid).is.eq(10)
            // expect(maker.ask).is.eq(10)
            // expect(maker.last).is.eq(10)
        })
    })
})
